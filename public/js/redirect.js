// Redirect insecure and WWW subdomain requests
if (location.protocol !== 'https:')
{
  location.href = 'https://paulwand.com';
}