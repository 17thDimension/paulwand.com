(function(){
    var waves = new SineWaves({
      el: document.getElementById('waves'),
      speed: 2,
      rotate: 0,
      ease: 'SineInOut',
      wavesWidth: '100%',
      waves: [
          {
            timeModifier: 1,
            lineWidth: 1,
            amplitude: -20,
            wavelength: 20,
          },
          {
            timeModifier: 1,
            lineWidth: 1,
            amplitude: -30,
            wavelength: 30,
          },
          {
            timeModifier: 1,
            lineWidth: 1,
            amplitude: -30,
            wavelength: 30,
          },
          {
            timeModifier: 1,
            lineWidth: 1,
            amplitude: -40,
            wavelength: 40,
          },
          {
            timeModifier: 1,
            lineWidth: 1,
            amplitude: -40,
            wavelength: 40,
          },
          {
            timeModifier: 1,
            lineWidth: 1,
            amplitude: -50,
            wavelength: 50,
          },
          {
            timeModifier: 1,
            lineWidth: 1,
            amplitude: -50,
            wavelength: 50,
          },
          {
            timeModifier: 1,
            lineWidth: 1,
            amplitude: -50,
            wavelength: 50,
          },
          {
            timeModifier: 1,
            lineWidth: 1,
            amplitude: -60,
            wavelength: 60,
          },
          {
            timeModifier: 1,
            lineWidth: 1,
            amplitude: -70,
            wavelength: 70,
          }],
    initialize: function (){},
    resizeEvent: function() {
      var gradient = this.ctx.createLinearGradient(0, 0, this.width, 0);
      gradient.addColorStop(0,"rgba(0, 0, 255, 0)");
      gradient.addColorStop(0.5,"rgba(255, 0, 255, 0.75)");
      gradient.addColorStop(1,"rgba(255, 0, 0, 0");
      var index = -1;
      var length = this.waves.length;
        while(++index < length){
        this.waves[index].strokeStyle = gradient;
      }
    }
    });
  })();

