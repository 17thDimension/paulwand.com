+++
title = "Emergence"
date = "2020-04-17"
image = 'emergence.jpg'
+++

A Fractal explorer for VR written in the unreal engine game framework.

![screenshot](https://steamcdn-a.akamaihd.net/steam/apps/500470/ss_ed36bb32fe07b01ad38259c12583e1c89d5c2aed.600x338.jpg?t=1589153086 "Screenshot")

<iframe src="https://store.steampowered.com/widget/500470/" frameborder="0" width="646" height="190"></iframe>