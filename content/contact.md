+++
title = "Contact"
date = "2014-04-09"
image = 'contact.jpg'
+++

If you'd like to arrange a phone discussion with me, 
please use Calendly:

<!-- Calendly inline widget begin -->
<div class="calendly-inline-widget" data-url="https://calendly.com/paul-wand/15min" style="min-width:320px;height:880px;"></div>
<script type="text/javascript" src="https://assets.calendly.com/assets/external/widget.js"></script>
<!-- Calendly inline widget end -->