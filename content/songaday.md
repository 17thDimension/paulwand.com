+++
title = "Song a Day"
date = "2014-04-09"
image = 'songaday.png'
+++

Song a day is a social network for prolific musical artists.
Over 5000 songs uploaded.
Community is active in January and July.


Website is here: [Song a Day](https://songadayforamonth.com).

Native iOS app is in beta:
[Song a Day iOS](https://testflight.apple.com/join/lqY1Xxgy?fbclid=IwAR3Co9in6dM3USr9isF3XokS_jxnauAU6WR1q0xHHZIr2ktuIk09LXYfgc8)