+++
title = "Numundo"
date = "2013-04-09"
image = 'numundo.jpg'
+++

Travel booking startup that I wrote some of the foundational code for.
Working with numundo is wonderful, I enjoyed the experience of startup culture in Chile.
My most recent contribution to the codebase has been Crypto payment functionality.
Coworkers from this company have now become good friends.

[Numundo](https://numundo.org/).

